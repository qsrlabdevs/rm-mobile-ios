import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/testqsr/Downloads/Regina Maria 3.app', false)

//Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - patientIcon'), 0)

//Mobile.setText(findTestObject('RM/000001/XCUIElementTypeTextField - Email'), 'rodica.grindei@reginamaria.ro', 0)

//Mobile.setText(findTestObject('RM/000001/XCUIElementTypeSecureTextField - Parola'), 'Cioran2019#', 0)

//Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - Toolbar Done Button'), 0)

//Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - Intra in cont'), 0)

//Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - closeIcon'), 0)

Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - redirectAccounts'), 0)

Mobile.tap(findTestObject('RM/000001/XCUIElementTypeSwitch - PT PANTECE TUDOR ANDREI Copil'), 0)

Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - Confirma'), 0)

Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - Programari'), 0)

IOSUtils.scrollToElement("Detalii")

Mobile.tap(findTestObject('RM/000001/XCUIElementTypeButton - Detalii'), 0)

Mobile.tap(findTestObject('RM/T162333/XCUIElementTypeButton - CHECK-IN'), 0)

Mobile.tap(findTestObject('RM/T162333/XCUIElementTypeButton - CHECK-IN ONLINE'), 0)

Mobile.closeApplication()