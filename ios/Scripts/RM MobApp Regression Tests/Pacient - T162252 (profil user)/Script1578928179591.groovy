import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.ios.IOSDriver as IOSDriver
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeStaticText - Profilul meu'), 0)

IOSUtils.scrollToElement("XCUIElementTypeButton - modify button textfield")

//Mobile.scrollToText('Telefon', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - modify button textfield'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - textfield save icon'), 0)

Mobile.scrollToText('Email', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - modify button textfield (1)'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - textfield save icon (1)'), 0)

Mobile.scrollToText('MODIFICA DATE PERSONALE', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeStaticText - SCHIMBA PAROLA'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - Anuleaza'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeStaticText - CONSIMTAMINTE'), 0)

Mobile.scrollToText('Salveaza', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - Salveaza'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - Continua'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - backIcon'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeStaticText - MODIFICA DATE PERSONALE'), 0)

Mobile.tap(findTestObject('RM/T162252/XCUIElementTypeButton - Anuleaza (1)'), 0)

Mobile.closeApplication()

