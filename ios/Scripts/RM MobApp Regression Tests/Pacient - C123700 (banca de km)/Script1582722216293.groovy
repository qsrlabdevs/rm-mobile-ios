import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.ios.IOSDriver as IOSDriver
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils

Mobile.startApplication('/Users/testqsr/Downloads/Regina Maria 3.app', false)

Mobile.tap(findTestObject('Object Repository/RM/C123700/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('Object Repository/RM/C123700/XCUIElementTypeStaticText - Banca de km'), 0)

//scroll not working
IOSUtils.scrollToElement("Sincronizeaza cu alte aplicatii")

Mobile.tap(findTestObject('RM/C123700/XCUIElementTypeButton - Sincronizeaza cu alte aplicatii'), 0)

Mobile.getText(findTestObject('RM/C123700/XCUIElementTypeStaticText - HealthKit'), 0)

Mobile.getText(findTestObject('RM/C123700/XCUIElementTypeStaticText - Fitbit'), 0)

Mobile.getText(findTestObject('RM/C123700/XCUIElementTypeStaticText - Strava'), 0)

Mobile.tap(findTestObject('RM/C123700/XCUIElementTypeSwitch - 0'), 0)

Mobile.tap(findTestObject('RM/C123700/XCUIElementTypeButton - Done'), 0)

Mobile.closeApplication()

