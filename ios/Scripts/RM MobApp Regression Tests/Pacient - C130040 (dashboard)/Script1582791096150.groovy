import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/testqsr/Downloads/Regina Maria 3.app', false)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - Programari'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeStaticText - Adauga o programare'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - Home'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - Dosar'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - ConsultatiiBigIcon'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton'), 0)

//Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - Nu acum'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - Dosar'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - investigatiiBigIcon'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - add programare'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton - Dosar'), 0)

Mobile.tap(findTestObject('RM/C130040/XCUIElementTypeButton'), 0)

Mobile.closeApplication()

