import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.ios.IOSDriver as IOSDriver
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - Programari'), 0)

Mobile.tap(findTestObject('RM/T162304/XCUIElementTypeStaticText - Adauga o programare'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - Fa-ti o programare'), 0)

IOSUtils.scrollToElement("Sunt de acord")

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - Sunt de acord'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - Judet'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - Bucuresti'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - Specialitatea'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - CHIRURGIE GENERALA'), 0)

Mobile.tap(findTestObject('RM/T162246/Nu acum'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - Incepand cu data'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - OK'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - Cauta'), 0)

Mobile.delay(3)

//IOSUtils.scrollToElement("1800")

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - 1400'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - consult chirurgie generala'), 0)

Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - Anuleaza'), 0)

//Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeStaticText - consult chirurgie vasculara'), 0)

//Mobile.tap(findTestObject('RM/T162357/XCUIElementTypeButton - Anuleaza'), 0)

Mobile.closeApplication()

