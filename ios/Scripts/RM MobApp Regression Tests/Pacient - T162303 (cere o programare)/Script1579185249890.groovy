import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.ios.IOSDriver as IOSDriver
import io.qsrlabs.katalon.utils.IOSUtils as IOSUtils

Mobile.startApplication('/Users/testqsr/Downloads/Regina_Maria-2.ipa', false)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - Programari'), 0)

Mobile.tap(findTestObject('RM/T162304/XCUIElementTypeStaticText - Adauga o programare'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - Fa-ti o programare'), 0)

IOSUtils.scrollToElement("Sunt de acord")

//IOSUtils.scrollDown(IOSUtils.XCUIElementTypeCollectionView);

//Mobile.scrollToText('RM/T162303/XCUIElementTypeButton - Sunt de acord', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - Sunt de acord'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - Judet'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - Bucuresti'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - Specialitatea'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - CARDIOLOGIE'), 0)

Mobile.tap(findTestObject('Object Repository/RM/T162303/XCUIElementTypeButton - Nu acum'), 0)

//Mobile.tap(findTestObject('Object Repository/RM/T162303/XCUIElementTypeStaticText'), 0)
Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - Incepand cu data'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - OK'), 0)

Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - Cauta'), 0)

NECESITA SCENARIU SPECIAL cu ora 18:00
//Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - 1800'), 0)

//Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeStaticText - consult cardiologie'), 0)

//Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - Confirma'), 0)

//Mobile.tap(findTestObject('RM/T162303/XCUIElementTypeButton - Ok (1)'), 0)

Mobile.closeApplication()

