import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/testqsr/Downloads/Regina Maria 3.app', false)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeButton - Item'), 0)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeButton - Iesi din cont'), 0)

//Mobile.closeApplication()

//Mobile.startApplication('/Users/testqsr/Downloads/Regina Maria 3.app', false)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeStaticText - Ma autentific mai tarziu'), 0)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeButton - Programari'), 0)

Mobile.tap(findTestObject('RM/T162304/XCUIElementTypeStaticText - Adauga o programare'), 0)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeButton - Fa-ti o programare'), 0)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeButton - Intra in cont'), 0)

Mobile.tap(findTestObject('RM/T162325/XCUIElementTypeButton - Intra in cont (1)'), 0)

Mobile.closeApplication()

